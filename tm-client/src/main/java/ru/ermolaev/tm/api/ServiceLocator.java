package ru.ermolaev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
