package ru.ermolaev.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;

public final class DataBinarySaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-bin-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminDataEndpoint().saveBinary(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
