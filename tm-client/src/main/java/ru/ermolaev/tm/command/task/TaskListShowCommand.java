package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.endpoint.Task;

import java.util.List;

public final class TaskListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint().showAllTasks(session);
        if (tasks == null) return;
        for (@NotNull final Task task: tasks) {
            System.out.println((tasks.indexOf(task) + 1)
                    + ". {id: "
                    + task.getId()
                    + "; name: "
                    + task.getName()
                    + "; description: "
                    + task.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
