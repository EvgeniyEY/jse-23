package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getTaskEndpoint().removeTaskById(session, id);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
