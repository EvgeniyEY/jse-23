package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminUserEndpoint().unlockUserByLogin(session, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
