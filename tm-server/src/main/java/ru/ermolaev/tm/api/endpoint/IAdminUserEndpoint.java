package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;

public interface IAdminUserEndpoint {

    @Nullable
    User createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @Nullable
    User createUserWithRole(@Nullable Session session, @Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User lockUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    @Nullable
    User unlockUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    @Nullable
    User removeUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

}
