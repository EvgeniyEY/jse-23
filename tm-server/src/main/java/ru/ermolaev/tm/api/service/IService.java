package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    void load(@Nullable List<T> ts);

    void load(@Nullable T... ts);

}
