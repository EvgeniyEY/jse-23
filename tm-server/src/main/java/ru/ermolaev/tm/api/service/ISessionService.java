package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password) throws Exception;

    boolean isValid(@Nullable Session session);

    void validate(@Nullable Session session) throws Exception;

    void validate(@Nullable Session session, @Nullable Role role) throws Exception;

    @Nullable
    Session open(@Nullable String login, @Nullable String password) throws Exception;

    @Nullable
    Session sign(@Nullable Session session);

    @Nullable
    User getUser(@Nullable Session session) throws Exception;

    @Nullable
    String getUserId(@Nullable Session session) throws Exception;

    @NotNull
    List<Session> getSessionList(@Nullable Session session) throws Exception;

    void close(@Nullable Session session) throws Exception;

    void closeAll(@Nullable Session session) throws Exception;

    void signOutByLogin(@Nullable String login) throws Exception;

    void signOutByUserId(@Nullable String userId) throws Exception;

}
