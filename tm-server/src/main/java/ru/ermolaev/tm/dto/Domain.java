package ru.ermolaev.tm.dto;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public final class Domain implements Serializable {

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

    @NotNull
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

    @NotNull
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(@NotNull final List<Project> projects) {
        this.projects = projects;
    }

    @NotNull
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(@NotNull final List<User> users) {
        this.users = users;
    }

}
