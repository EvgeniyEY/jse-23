package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.IProjectEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint implements IProjectEndpoint {

    private ServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().createProject(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAllProjects(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> showAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllProjects(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Project showProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project showProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeProjectByName(session.getUserId(), name);
    }

}
