package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;

import java.util.Arrays;
import java.util.List;

public class SessionRepositoryTest {

    final private ISessionRepository sessionRepository = new SessionRepository();

    final private static Session sessionOne = new Session();

    final private static Session sessionTwo = new Session();

    final private static Session sessionThree = new Session();

    final private static Session[] sessionArray = {sessionOne, sessionTwo, sessionThree};

    final private static List<Session> sessionList = Arrays.asList(sessionArray);

    @BeforeClass
    public static void initData() {
        sessionOne.setSignature("21hb31h24vh12v4");
        sessionOne.setStartTime(12312421L);
        sessionOne.setUserId("123123123");

        sessionTwo.setSignature("211sdfsf232y854vh12v4");
        sessionTwo.setStartTime(325126421L);
        sessionTwo.setUserId("642363426");

        sessionThree.setSignature("2f3523igbyuerb2v4");
        sessionThree.setStartTime(1235253221L);
        sessionThree.setUserId("642363426");
    }

    @Before
    public void addProject() {
        sessionRepository.add(sessionOne);
    }

    @After
    public void deleteProject() {
        sessionRepository.clear();
    }

    @Test
    public void findByIdTest() throws Exception {
        final Session session = sessionRepository.findById(sessionOne.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(sessionOne.getId(), session.getId());
    }

    @Test
    public void findByUserIdTest() throws Exception {
        final Session session = sessionRepository.findByUserId(sessionOne.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(sessionOne.getId(), session.getId());
    }

    @Test
    public void removeByUserIdTest() throws Exception {
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionRepository.removeByUserId(sessionOne.getUserId());
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    public void containsTest() throws Exception {
        Assert.assertTrue(sessionRepository.contains(sessionOne.getId()));
    }

    @Test
    public void findAllSessionsTest() {
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionRepository.add(sessionList);
        Assert.assertEquals(4, sessionRepository.findAll().size());
        Assert.assertEquals(2, sessionRepository.findAllSessions(sessionThree.getUserId()).size());
    }

    @Test
    public void addTest() throws Exception {
        sessionRepository.add(sessionThree.getUserId(), sessionThree);
        Session session = sessionRepository.findByUserId(sessionThree.getUserId());
        Assert.assertEquals(session.getId(), sessionThree.getId());
    }

    @Test
    public void removeTest() {
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionRepository.remove(sessionOne.getUserId(),sessionOne);
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    public void clearTest() {
        sessionRepository.add(sessionArray);
        Assert.assertEquals(4, sessionRepository.findAll().size());
        sessionRepository.clear();
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test(expected = UnknownIdException.class)
    public void findByIdExceptionTest() throws Exception {
        sessionRepository.findById("hdvas");
    }

    @Test(expected = UnknownIdException.class)
    public void findByUserIdExceptionTest() throws Exception {
        sessionRepository.findByUserId("qwerw");
    }

    @Test(expected = UnknownIdException.class)
    public void removeByUserIdExceptionTest() throws Exception {
        sessionRepository.removeByUserId("qwrq");
    }

    @Test(expected = UnknownIdException.class)
    public void containsExceptionTest() throws Exception {
        sessionRepository.contains("qwrqw");
    }

}
