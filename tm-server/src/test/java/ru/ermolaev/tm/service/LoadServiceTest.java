package ru.ermolaev.tm.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

public class LoadServiceTest {

    final private static Bootstrap bootstrap = new Bootstrap();

    @BeforeClass
    public static void prepareData() throws Exception {
        bootstrap.getTaskService().createTask("1", "11");
        bootstrap.getTaskService().createTask("2", "22");
        bootstrap.getProjectService().createProject("1", "11");
        bootstrap.getProjectService().createProject("2", "22");
        bootstrap.getUserService().create("1", "11");
        bootstrap.getUserService().create("2", "22");
    }

    @Test
    public void saveLoadBase64Test() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveBase64();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadBase64();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void saveLoadBinaryTest() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveBinary();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadBinary();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void saveLoadJsonByFasterXmlTest() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveJsonByFasterXml();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadJsonByFasterXml();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void saveLoadJsonByJaxbTest() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveJsonByJaxb();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadJsonByJaxb();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void saveLoadXmlByFasterXmlTest() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveXmlByFasterXml();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadXmlByFasterXml();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void saveLoadXmlByJaxbTest() throws Exception {
        Assert.assertEquals(2, bootstrap.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrap.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrap.getUserService().findAll().size());
        bootstrap.getLoadService().saveXmlByJaxb();
        final Bootstrap bootstrapForTest = new Bootstrap();
        bootstrapForTest.getLoadService().loadXmlByJaxb();
        Assert.assertEquals(2, bootstrapForTest.getTaskService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getProjectService().findAll().size());
        Assert.assertEquals(2, bootstrapForTest.getUserService().findAll().size());
    }

    @Test
    public void clearBase64FileTest() throws Exception {
        bootstrap.getLoadService().saveBase64();
        final File file = new File(DataConstant.FILE_BASE64);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearBase64File();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    public void clearBinaryFileTest() throws Exception {
        bootstrap.getLoadService().saveBinary();
        final File file = new File(DataConstant.FILE_BINARY);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearBinaryFile();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    public void clearJsonFileJaxbTest() throws Exception {
        bootstrap.getLoadService().saveJsonByJaxb();
        final File file = new File(DataConstant.FILE_JSON_JB);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearJsonFileJaxb();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    public void clearJsonFileFasterXmlTest() throws Exception {
        bootstrap.getLoadService().saveJsonByFasterXml();
        final File file = new File(DataConstant.FILE_JSON_FX);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearJsonFileFasterXml();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    public void clearXmlFileFasterXmlTest() throws Exception {
        bootstrap.getLoadService().saveXmlByFasterXml();
        final File file = new File(DataConstant.FILE_XML_FX);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearXmlFileFasterXml();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    public void clearXmlFileJaxbTest() throws Exception {
        bootstrap.getLoadService().saveXmlByJaxb();
        final File file = new File(DataConstant.FILE_XML_JB);
        Assert.assertTrue(Files.exists(file.toPath()));
        bootstrap.getLoadService().clearXmlFileJaxb();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

}
